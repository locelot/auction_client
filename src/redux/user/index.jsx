/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'

const authInfo = JSON.parse(localStorage.getItem('authTokens') || '{}')
const user = JSON.parse(localStorage.getItem('user') || '{}')

export const userSlice = createSlice({
	name: 'user',
	initialState: {
		isAuth: !!authInfo.accessToken,
		data: { ...user }
	},
	reducers: {
		LogIn: (state, payload) => {
			state.data = { ...payload.payload }
			state.isAuth = true
		},
		LogOut: state => {
			state.isAuth = false
		}
	}
})

export const { LogIn, LogOut } = userSlice.actions

export default userSlice.reducer
