// import { useState } from 'react'
// import { useForm } from 'react-hook-form'
// import { joiResolver } from '@hookform/resolvers/joi'
// import Joi from 'joi'
// import { toast } from 'react-toastify'

// import ImageUploader from '../../UI/ImageUploader'

// import styles from './style.module.scss'
// import http from '../../shared/http'
// import { TApi } from '../../shared/const'
// import { joiFile } from '../../shared/helpers'

// const schema = Joi.object({
// 	image: joiFile(10)
// })

// export default function imageUploader() {
// 	const {
// 		register,
// 		handleSubmit,
// 		clearErrors,
// 		formState: { errors }
// 	} = useForm({
// 		reValidateMode: 'onChange',
// 		resolver: joiResolver(schema)
// 	})
// 	const [imageSrc, setImageSrc] = useState('')

// 	const handleChange = async fields => {
// 		try {
// 			const form = new FormData()
// 			if (fields.image && fields.image[0]) {
// 				form.append('image', fields.image[0])
// 			}
// 			setImageSrc(URL.createObjectURL(fields.image[0]))
// 			await http.post(TApi.UPLOAD_ITEM_IMAGE, form)
// 			toast.success('Image has been successfully uploaded')
// 		} catch (err) {
// 			const { message } = err.response.data
// 			toast.error(message)
// 		}
// 	}

// 	const handleClean = () => {
// 		if (imageSrc) {
// 			toast.success('Image successfully deleted')
// 			setImageSrc('')
// 			clearErrors('Image')
// 			//   axios.delete(TApi.STUDENT_Image_DELETE).then(() => {
// 			//     toast.success('Image successfully deleted');
// 			//     setImageSrc('');
// 			//     clearErrors('Image');
// 			//   });
// 		}
// 	}

// 	return (
// 		<div className={styles.root}>
// 			<ImageUploader
// 				src={imageSrc}
// 				error={errors.image}
// 				onClean={handleClean}
// 				onChange={handleSubmit(handleChange)}
// 				className={styles.ImageUploader}
// 				register={register('image')}
// 			/>
// 		</div>
// 	)
// }
