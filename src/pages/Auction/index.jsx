import { useState } from 'react'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import { useSelector } from 'react-redux'
import CreateItem from './CreateAuctionItem'
import ListAuctionItems from '../ListAuctionItems'

function TabPanel(props) {
	const { children, value, index, ...other } = props

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}>
			{value === index && (
				<Box sx={{ p: 3 }}>
					<Typography>{children}</Typography>
				</Box>
			)}
		</div>
	)
}

function a11yProps(index) {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`
	}
}

export default function BasicTabs(props) {
	const { auction } = props.location.state
	const userId = useSelector(state => state.user.data.id)
	const [value, setValue] = useState(0)

	const handleChange = (event, newValue) => {
		setValue(newValue)
	}

	return (
		<Box sx={{ width: '100%', marginTop: '70px' }}>
			<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
				<Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
					<Tab label="Items" {...a11yProps(0)} />
					<Tab label="Create item" {...a11yProps(1)} disabled={auction.isEnded || !(userId === auction.creatorId)} />
				</Tabs>
			</Box>
			<TabPanel value={value} index={0}>
				<ListAuctionItems auction={auction} />
			</TabPanel>
			<TabPanel value={value} index={1}>
				<CreateItem
					auctionId={auction && auction.id}
					changeTab={() => {
						setValue(0)
					}}
				/>
			</TabPanel>
		</Box>
	)
}
