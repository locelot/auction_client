import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import ListSubheader from '@mui/material/ListSubheader'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import DashboardIcon from '@mui/icons-material/Dashboard'
import AssignmentIcon from '@mui/icons-material/Assignment'
import { Link } from 'react-router-dom'

import { TRoutes } from '../../../shared/const'

export function renderMainItems() {
	const mainItems = [
		{
			id: 1,
			name: 'Main',
			Icon: <DashboardIcon />,
			to: TRoutes.AUCTION_LIST
		},
		{
			id: 2,
			name: 'Profile',
			Icon: <AccountCircleIcon />,
			to: TRoutes.PROFILE
		}
	]
	return mainItems.map(item => (
		<Link key={item.id} to={item.to} style={{ textDecoration: 'none', color: 'black' }}>
			<ListItemButton>
				<ListItemIcon>{item.Icon}</ListItemIcon>

				<ListItemText primary={item.name} />
			</ListItemButton>
		</Link>
	))
}

export const secondaryListItems = (
	<>
		<ListSubheader component="div" inset>
			Saved reports
		</ListSubheader>
		<ListItemButton>
			<ListItemIcon>
				<AssignmentIcon />
			</ListItemIcon>
			<ListItemText primary="Current month" />
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<AssignmentIcon />
			</ListItemIcon>
			<ListItemText primary="Last quarter" />
		</ListItemButton>
		<ListItemButton>
			<ListItemIcon>
				<AssignmentIcon />
			</ListItemIcon>
			<ListItemText primary="Year-end sale" />
		</ListItemButton>
	</>
)
