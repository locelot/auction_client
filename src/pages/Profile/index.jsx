import { useEffect, useState } from 'react'
import Joi from 'joi'
import clsx from 'clsx'
import { joiResolver } from '@hookform/resolvers/joi'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import http from '../../shared/http'
import { TApi } from '../../shared/const'
import styles from './style.module.scss'
import DateTimeField from '../../UI/DateTimeField'
import TextField from '../../UI/TextField'
import AvatarUploader from './Avatar'

const schema = Joi.object({
	firstname: Joi.string().required(),
	surname: Joi.string().required(),
	dateOfBirth: Joi.date().required(),
	address: Joi.string().required()
})

export default function Profile() {
	const {
		register,
		handleSubmit,
		reset,
		control,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})
	const [profileData, setProfileData] = useState({})
	const [isLoading, setLoading] = useState(false)

	useEffect(() => {
		async function fetchData() {
			try {
				setLoading(true)
				const { data } = await http.get(TApi.GET_PROFILE)
				const { profile } = data.message
				setProfileData(profile)

				reset({
					firstname: profile.firstname,
					surname: profile.surname,
					dateOfBirth: profile.dateOfBirth && new Date(profile.dateOfBirth),
					address: profile.address
				})
			} catch (err) {
				const { message } = err.response.data
				toast.error(message)
			} finally {
				setLoading(false)
			}
		}

		fetchData()
	}, [])

	const onSubmit = async form => {
		try {
			setLoading(true)

			await http.post(TApi.UPDATE_PROFILE, {
				firstname: form.firstname,
				surname: form.surname,
				dateOfBirth: form.dateOfBirth,
				address: form.address
			})

			toast.success('Profile has been updated!')
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		} finally {
			setLoading(false)
		}
	}

	function subtractYears(numOfYears, date = new Date()) {
		date.setFullYear(date.getFullYear() - numOfYears)

		return date
	}

	return (
		<div className={styles.root}>
			<AvatarUploader />
			<form className={clsx(styles.miniContainer, styles.form)} onSubmit={handleSubmit(onSubmit)} autoComplete="off">
				<TextField mb={22} value={profileData.username} label="Username" placeholder="Your username" disabled />
				<div className={styles.flexContainer}>
					<TextField mb={22} value={profileData.email} label="Email" placeholder="Your email" disabled />
					<TextField mb={22} value={profileData.phonenumber} label="Phone" placeholder="Your phone" disabled />
				</div>

				<div className={styles.flexContainer}>
					<TextField
						mb={22}
						label="First name"
						placeholder="Your first name"
						error={errors.firstname}
						register={register('firstname')}
					/>
					<TextField
						mb={22}
						label="Surname"
						placeholder="Your surname"
						error={errors.surname}
						register={register('surname')}
					/>
					<DateTimeField
						mb={22}
						name="dateOfBirth"
						label="Birth date"
						placeholder="01.01.2001"
						error={errors.dateOfBirth}
						control={control}
						dateFormat="dd.MM.yyyy"
						maxDate={subtractYears(18)}
					/>
				</div>
				<div className={styles.flexContainer}>
					<TextField
						mb={22}
						label="Address"
						placeholder="Your address"
						error={errors.address}
						register={register('address')}
					/>
				</div>
				<Button type="submit" variant="contained" disabled={isLoading}>
					Update profile
				</Button>
			</form>
		</div>
	)
}
