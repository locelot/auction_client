import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import clsx from 'clsx'
import DeleteIcon from '@mui/icons-material/AutoDelete'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'
import Button from '@mui/material/Button'
import http from '../../../shared/http'
import { TRoutes, TApi } from '../../../shared/const'
import { ReactComponent as AuctionSVG } from '../../../assets/images/auction-default.svg'
import styles from '../style.module.scss'
import MyTimer from '../../../UI/Timer'

export default function AuctionPreview(props) {
	const history = useHistory()
	const { auction, onDelete } = props
	const userId = useSelector(state => state.user.data.user_id)
	const [images, setImages] = useState([])
	const [active, setActive] = useState(auction.active)
	const [isEnded, setEnded] = useState(auction.isEnded)

	async function removeAuction() {
		try {
			await http.post(TApi.REMOVE_AUCTION, {
				auctionId: auction.id
			})
			onDelete(auction.id)
			toast.success('Auction has been removed!')
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}

	useEffect(() => {
		async function fetchData() {
			try {
				const { data } = await http.get(TApi.GET_ALL_ITEM_IMAGES, {
					params: { auctionId: auction.id }
				})

				setImages(data.imagesBase64)
			} catch (err) {
				const { message } = err.response.data
				toast.error(message)
			}
		}
		fetchData()
	}, [])

	function renderImages(data) {
		return data.map(img => (
			<div style={{ padding: 10 }}>
				<img style={{ height: '100%', maxWidth: 250 }} src={`data:image/png;base64,${img}`} alt="item" />
			</div>
		))
	}

	async function handleClick() {
		try {
			if (!auction.password) {
				history.push({
					pathname: TRoutes.AUCTION,
					state: {
						auction: {
							...auction,
							active,
							isEnded
						}
					}
				})
				return
			}
			const password = prompt('Enter a password please: ')
			await http.post(TApi.GET_AUCTION_ACCESS, {
				auctionId: auction.id,
				password
			})

			history.push({
				pathname: TRoutes.AUCTION,
				state: {
					auction: {
						...auction,
						active,
						isEnded
					}
				}
			})
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}

	return (
		// eslint-disable-next-line react/jsx-no-useless-fragment
		<>
			{auction && (
				<div className={styles.auctionBlock}>
					<div className={styles.images}>
						<span className={styles.deleteIcon}>
							{!active && userId === auction.creator_id && (
								<DeleteIcon
									color="error"
									onClick={() => {
										const confirmation = confirm('Are you sure you want to delete auction?')
										if (!confirmation) return
										removeAuction()
									}}
								/>
							)}
						</span>
						{images.length > 0 ? renderImages(images) : <AuctionSVG />}
					</div>
					<div className={styles.info}>
						<span className={clsx(styles.infoBlock, styles.infoName)}>{auction.auction_name}</span>
						<div
							style={{
								display: 'flex',
								alignItems: 'center',
								justifyContent: 'space-between'
							}}>
							<div
								style={{
									display: 'flex',
									flexDirection: 'column'
								}}>
								<span className={styles.infoDate}>
									Start:
									{` ${new Date(auction.start_date).toLocaleString()}`}
								</span>
								<span className={styles.infoDate}>
									End:
									{` ${new Date(auction.end_date).toLocaleString()}`}
								</span>
							</div>

							<Button variant="contained" onClick={handleClick}>
								Explore
							</Button>
							{/* {!auction.isEnded && userId === auction.creator_id && (
								<Modal component={Test} regular auctionId={auction.auction_id} auctionEndDate={auction.endDate} />
							)} */}
						</div>
						{active && (
							<MyTimer
								expiryTimestamp={new Date(auction.end_date)}
								onExpire={() => {
									setActive(false)
									setEnded(true)
								}}
								className={styles.timer}
							/>
						)}
					</div>
				</div>
			)}
		</>
	)
}
