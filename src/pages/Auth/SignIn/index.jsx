import { useState } from 'react'
import { useDispatch } from 'react-redux'
import Joi from 'joi'
import { Link, useHistory } from 'react-router-dom'
import clsx from 'clsx'
import { joiResolver } from '@hookform/resolvers/joi'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import { LogIn } from '../../../redux/user'
import { TRoutes, TApi, API_URL } from '../../../shared/const'
import TextField from '../../../UI/TextField'

import styles from './style.module.scss'

const schema = Joi.object({
	login: Joi.string().required(),
	password: Joi.string().required()
})

function SignIn() {
	const history = useHistory()
	const {
		register,
		handleSubmit,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})
	const [isLoading, setLoading] = useState(false)
	const dispatch = useDispatch()
	const onSubmit = async form => {
		try {
			setLoading(true)
			const result = await axios.post(API_URL + TApi.SIGN_IN, {
				login: form.login,
				password: form.password
			})
			const { accessToken, refreshToken } = result.data.message
			const { user } = result.data

			localStorage.setItem('authTokens', JSON.stringify({ accessToken, refreshToken }))
			localStorage.setItem('user', JSON.stringify(user))
			dispatch(LogIn(user))
			toast.success('Welcome')
			history.go(TRoutes.AUCTION_LIST)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		} finally {
			setLoading(false)
		}
	}

	return (
		<div className={styles.root}>
			<div className={styles.header}>
				<div className={styles.miniContainer}>
					<h2 className={styles.titleSignIn}>Sign In</h2>
					<p className={styles.accountMessage}>
						Do not have an account yet?
						<Link to={TRoutes.SIGN_UP}> Sign Up</Link>
					</p>
				</div>
			</div>
			<form className={clsx(styles.miniContainer, styles.form)} onSubmit={handleSubmit(onSubmit)} autoComplete="off">
				<TextField
					mb={22}
					label="Login"
					placeholder="Your email or username"
					error={errors.login}
					register={register('login')}
					required
				/>
				<TextField
					mb={22}
					type="password"
					label="Password"
					placeholder="Your password"
					error={errors.password}
					register={register('password')}
					required
				/>
				<p className={styles.forgotPassportMessage}>
					Forgot password?
					<Link to={TRoutes.RECOVER_PASS}> Recover Pass</Link>
				</p>
				<Button type="submit" variant="contained" disabled={isLoading}>
					Sign In
				</Button>
			</form>
		</div>
	)
}

export default SignIn
