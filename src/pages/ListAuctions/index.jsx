import { useState, useEffect } from 'react'
import AuctionPreview from './AuctionPreview'
import { joiResolver } from '@hookform/resolvers/joi'
import { useForm, useWatch } from 'react-hook-form'
import Joi from 'joi'
import { toast } from 'react-toastify'
import SwitchBtn from '../../UI/SwitchBtn'
import http from '../../shared/http'
import { TApi } from '../../shared/const'
import { ReactComponent as NotFoundSVG } from '../../assets/icons/data-not-found.svg'
import styles from './style.module.scss'
import TextField from '../../UI/TextField'
import DateTimeField from '../../UI/DateTimeField'

// function Test(props) {
// 	const schema = Joi.object({
// 		endDate: Joi.date().greater(new Date(props.auctionEndDate).getTime()).required()
// 	})

// 	const {
// 		control,
// 		handleSubmit,
// 		formState: { errors }
// 	} = useForm({
// 		resolver: joiResolver(schema)
// 	})

// 	async function onSubmit(form) {
// 		try {
// 			await http.post(TApi.UPDATE_AUCTION, {
// 				auctionId: props.auctionId,
// 				endDate: form.endDate
// 			})
// 			toast.success('End time has been updated')
// 			props.handleClose()
// 		} catch (err) {
// 			const { message } = err.response.data
// 			toast.error(message)
// 		}
// 	}

// 	return (
// 		<div style={{ display: 'flex', width: '100%', justifyContent: 'center' }}>
// 			<form className={clsx(styles.miniContainer, styles.form)} onSubmit={handleSubmit(onSubmit)} autoComplete="off">
// 				<span>Сontinue end time of auction</span>
// 				<DateTimeField
// 					mb={20}
// 					name="endDate"
// 					label="New End Date"
// 					placeholder="01.01.2022"
// 					error={errors.endDate}
// 					control={control}
// 					showTimeSelect
// 					dateFormat="dd.MM.yyyy HH:mm"
// 					timeFormat="HH:mm"
// 				/>

// 				<Button type="submit" variant="contained" style={{ width: '100%' }}>
// 					Update
// 				</Button>
// 			</form>
// 		</div>
// 	)
// }

const schema = Joi.object({
	byUser: Joi.boolean(),
	byStartDate: Joi.string().required(),
	byEndDate: Joi.string().required()
})

function ListAuctions() {
	const {
		control,
		setValue,
		getValues,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})

	const [search, setSearch] = useState('')
	const [auctions, setAuctions] = useState([])

	const filters = useWatch({
		control,
		name: ['byUser', 'byStartDate', 'byEndDate']
	})

	useEffect(async () => {
		try {
			const filterOptions = getValues()

			const result = await http.get(TApi.GET_AUCTIONS, {
				params: { search, filter: JSON.stringify({ ...filterOptions }) }
			})

			setAuctions(result.data.message.auctions)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}, [search, filters])

	function handleDeleteAuction(auctionId) {
		const updatedAuctions = auctions.filter(e => e.id !== auctionId)
		setAuctions(updatedAuctions)
	}

	function dataList(data) {
		return data.map(auction => <AuctionPreview key={auction.id} auction={auction} onDelete={handleDeleteAuction} />)
	}

	return (
		<div className={styles.root}>
			<div className={styles.search}>
				<div className={styles.switch}>
					<SwitchBtn
						label="My auctions"
						labelPlacement="end"
						onChange={() => setValue('byUser', !getValues('byUser'))}
					/>
				</div>

				<div className={styles.filter}>
					<DateTimeField
						mb={20}
						name="byStartDate"
						label="Start Date"
						placeholder="01.01.2022"
						error={errors.byStartDate}
						control={control}
						showTimeSelect
						dateFormat="dd.MM.yyyy HH:mm"
						timeFormat="HH:mm"
					/>
					<DateTimeField
						mb={20}
						name="byEndDate"
						label="End Date"
						placeholder="01.01.2022"
						error={errors.byEndDate}
						control={control}
						showTimeSelect
						dateFormat="dd.MM.yyyy HH:mm"
						timeFormat="HH:mm"
					/>
				</div>
				<TextField placeholder="Search" onChange={e => setSearch(e.target.value)} />
			</div>

			<div className={styles.list}>{auctions.length ? dataList(auctions) : <NotFoundSVG style={{ width: 200 }} />}</div>
		</div>
	)
}

export default ListAuctions
