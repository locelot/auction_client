import { useState } from 'react'
import Joi from 'joi'
import { useHistory } from 'react-router-dom'
import clsx from 'clsx'
import { joiResolver } from '@hookform/resolvers/joi'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import { TRoutes, TApi, API_URL } from '../../shared/const'
import TextField from '../../UI/TextField'

import styles from './common.module.scss'

export const schema = Joi.object({
	email: Joi.string()
		.email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
		.required()
})

function RecoverPass() {
	const history = useHistory()
	const {
		register,
		handleSubmit,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})
	const [isLoading, setLoading] = useState(false)

	const onSubmit = async form => {
		try {
			setLoading(true)
			await axios.post(API_URL + TApi.REQUEST_PASSWORD_RESET, {
				email: form.email,
				type: 'password'
			})

			toast.success('Letter has been sent to your email')
			history.push(TRoutes.SIGN_IN)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		} finally {
			setLoading(false)
		}
	}

	return (
		<div className={styles.root}>
			<div className={styles.header}>
				<div className={styles.miniContainer}>
					<h2>Recover password </h2>
				</div>
			</div>
			<form className={clsx(styles.miniContainer, styles.form)} onSubmit={handleSubmit(onSubmit)} autoComplete="off">
				<TextField
					mb={22}
					label="Email"
					placeholder="Your email"
					error={errors.email}
					register={register('email')}
					required
				/>

				<Button type="submit" variant="contained" disabled={isLoading}>
					Confirm
				</Button>
			</form>
		</div>
	)
}

export default RecoverPass
