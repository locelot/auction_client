import ee from 'event-emitter'

export const Emitter = ee()

export const types = {
	openCreateAuction: 'openCreateAuction'
}
