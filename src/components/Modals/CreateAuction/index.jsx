/* eslint-disable prettier/prettier */
import { useState } from 'react'
import Joi from 'joi'
import { joiResolver } from '@hookform/resolvers/joi'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import { TApi } from '../../../shared/const'
import TextField from '../../../UI/TextField'
import DateTimeField from '../../../UI/DateTimeField'
import http from '../../../shared/http';

import styles from './style.module.scss'

const filterPassedTime = (time) => {
  const currentDate = new Date()
  const selectedDate = new Date(time)

  return currentDate.getTime() < selectedDate.getTime()
}

function customStartDate(value, helpers) {
  const date = new Date(value)
  const time = date.getTime()
  if (!filterPassedTime(time)) return helpers.message('Time must be greater then current!')
  return value
}

const schema = Joi.object({
  name: Joi.string().required(),
  startDate: Joi.date().custom(customStartDate).required(),
  endDate: Joi.date().greater(Joi.ref('startDate')).required(),
  password: Joi.string().allow('')
})

function CreateAuction(props) {
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    resolver: joiResolver(schema),
  })
  const [isLoading, setLoading] = useState(false)

  const onSubmit = async (form) => {
    try {
      setLoading(true)

      await http.post(TApi.CREATE_AUCTION, {
        name: form.name,
        startDate: form.startDate,
        endDate: form.endDate,
        password: form.password
      })
      props.handleClose()
      toast.success('Auction has been created')
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    } finally {
      setLoading(false)
    }
  }

  return (
    <div className={styles.root}>

      <form
        className={styles.form}
        onSubmit={handleSubmit(onSubmit)}
        autoComplete="off"
      >
        <TextField
          mb={22}
          label="Name"
          placeholder="Auction name"
          error={errors.name}
          register={register('name')}
          required
        />

        <DateTimeField
          disabled={isLoading}
          mb={20}
          name="startDate"
          label="Start date"
          placeholder="01.01.2022"
          error={errors.startDate}
          control={control}
          showTimeSelect
          dateFormat="dd.MM.yyyy HH:mm"
          timeFormat="HH:mm"
          minDate={new Date()}
          filterTime={filterPassedTime}
        />
        <DateTimeField
          disabled={isLoading}
          mb={20}
          name="endDate"
          label="End Date"
          placeholder="01.01.2022"
          error={errors.endDate}
          control={control}
          showTimeSelect
          dateFormat="dd.MM.yyyy HH:mm"
          timeFormat="HH:mm"
          minDate={new Date()}
        />

        <TextField
          mb={22}
          type="password"
          label="Protected"
          placeholder="Password"
          error={errors.password}
          register={register('password')}
        />

        <Button type="submit" variant="contained" disabled={isLoading}>
          Create Auction
        </Button>
      </form>
    </div>
  )
}

export default CreateAuction
