import { useState } from 'react'
import ListItemButton from '@mui/material/ListItemButton'
import LogoutIcon from '@mui/icons-material/Logout'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import { useSelector, useDispatch } from 'react-redux'
import { styled, createTheme, ThemeProvider } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import MuiDrawer from '@mui/material/Drawer'
import Box from '@mui/material/Box'
import MuiAppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import List from '@mui/material/List'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import IconButton from '@mui/material/IconButton'
import Badge from '@mui/material/Badge'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import NotificationsIcon from '@mui/icons-material/Notifications'
import { LogOut } from '../../redux/user'
import { renderMainItems } from './ListItems'
import Modal from '../../UI/Modal'
import AddCircleIcon from '@mui/icons-material/AddCircle'
import CreateAuction from '../Modals/CreateAuction'
import { Emitter as eventEmitter, types } from '../../shared/eventEmitter'

const drawerWidth = 240

const AppBar = styled(MuiAppBar, {
	shouldForwardProp: prop => prop !== 'open'
})(({ theme, open }) => ({
	zIndex: theme.zIndex.drawer + 1,
	transition: theme.transitions.create(['width', 'margin'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen
	}),
	...(open && {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	})
}))

const Drawer = styled(MuiDrawer, {
	shouldForwardProp: prop => prop !== 'open'
})(({ theme, open }) => ({
	'& .MuiDrawer-paper': {
		position: 'relative',
		whiteSpace: 'nowrap',
		width: drawerWidth,
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		}),
		boxSizing: 'border-box',
		...(!open && {
			overflowX: 'hidden',
			transition: theme.transitions.create('width', {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen
			}),
			width: theme.spacing(7),
			[theme.breakpoints.up('sm')]: {
				width: theme.spacing(9)
			}
		})
	}
}))

const mdTheme = createTheme()

export default function DashboardContent() {
	const dispatch = useDispatch()
	const isAuth = useSelector(state => state.user.isAuth)

	const [open, setOpen] = useState(false)
	const toggleDrawer = () => {
		setOpen(!open)
	}

	function backgroundColorHandler(theme) {
		return theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900]
	}
	function logOut() {
		localStorage.removeItem('authTokens')
		dispatch(LogOut())
	}

	// eslint-disable-next-line consistent-return
	return (
		<>
			{isAuth && (
				<ThemeProvider theme={mdTheme}>
					<Box sx={{ display: 'flex' }}>
						<CssBaseline />
						<AppBar position="absolute" open={open}>
							<Toolbar
								sx={{
									pr: '24px' // keep right padding when drawer closed
								}}>
								<IconButton
									edge="start"
									color="inherit"
									aria-label="open drawer"
									onClick={toggleDrawer}
									sx={{
										marginRight: '36px',
										...(open && { display: 'none' })
									}}>
									<MenuIcon />
								</IconButton>
								<Typography component="h1" variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
									LiveAuction
								</Typography>
								<IconButton color="inherit">
									<Badge badgeContent={4} color="secondary">
										<NotificationsIcon />
									</Badge>
								</IconButton>
							</Toolbar>
						</AppBar>
						<Drawer variant="permanent" open={open}>
							<Toolbar
								sx={{
									display: 'flex',
									alignItems: 'center',
									justifyContent: 'flex-end',
									px: [1]
								}}>
								<IconButton onClick={toggleDrawer}>
									<ChevronLeftIcon />
								</IconButton>
							</Toolbar>
							<Divider />
							<List component="nav">
								{renderMainItems()}
								<ListItemButton
									onClick={() => {
										eventEmitter.emit(types.openCreateAuction)
									}}>
									<ListItemIcon>
										<AddCircleIcon />
									</ListItemIcon>
									<ListItemText>Create auction</ListItemText>
								</ListItemButton>
								<Modal component={CreateAuction} eventName={types.openCreateAuction} />
								<ListItemButton onClick={logOut}>
									<ListItemIcon>
										<LogoutIcon />
									</ListItemIcon>
									<ListItemText>Log Out</ListItemText>
								</ListItemButton>
								<Divider sx={{ my: 1 }} />
							</List>
						</Drawer>
					</Box>
				</ThemeProvider>
			)}
		</>
	)
}
