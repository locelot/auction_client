import clsx from 'clsx'
import Avatar from 'react-avatar'
// import Button from '@mui/material/Button'
import styles from './style.module.scss'

export default function ImageUploader(props) {
	return (
		<div className={clsx(styles.root, props.className)}>
			<Avatar src={props.src} className={styles.preview} color="#e5eaf5" size={230} />
			<div className={styles.wrapper}>
				{props.error && <span className={styles.error}>{props.error.message}</span>}
				<div className={styles.wrapperInput}>
					<label className={clsx(styles.labelUploadFile, props.disabled && styles.labelUploadFileDisabled)}>
						<input
							{...(props.register || {})}
							disabled={props.disabled}
							className={styles.input}
							type="file"
							accept="image/jpeg,image/png,image/gif"
							onChange={evt => {
								if (props.register) props.register.onChange(evt)
								if (props.onChange) props.onChange(evt)
							}}
							onClick={event => {
								event.target.value = null
							}}
						/>

						<span>Upload profile photo</span>
					</label>

					{/* <Button type="submit" variant="contained" onClick={props.onClean}>
            Remove
          </Button> */}
				</div>
			</div>
		</div>
	)
}

ImageUploader.defaultProps = {
	disabled: false,
	onClean: () => {}
}
