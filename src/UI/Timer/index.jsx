/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-one-expression-per-line */
import { useTimer } from 'react-timer-hook'

export default function MyTimer({ expiryTimestamp, onExpire, className }) {
	const { seconds, minutes, hours, days } = useTimer({
		expiryTimestamp,
		onExpire: () => onExpire && onExpire()
	})

	return (
		<div style={{ textAlign: 'center' }}>
			<div className={className}>
				<span>{days}</span>:<span>{hours}</span>:<span>{minutes}</span>:<span>{seconds}</span>
			</div>
		</div>
	)
}
