/* eslint-disable prettier/prettier */
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'

export default function SwitchLabels(props) {
  return (
    <FormGroup>
      <FormControlLabel
        disabled={props.disabled}
        control={(
          <Switch
            onChange={(e) => {
              props.onChange(e)
            }}
          />
        )}
        label={props.label}
        labelPlacement={props.labelPlacement}
      />
    </FormGroup>
  )
}
