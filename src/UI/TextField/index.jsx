import { useState } from 'react'
import clsx from 'clsx'

import styles from './style.module.scss'

export default function TextField(props) {
	const [type] = useState(props.type ? props.type : 'text')
	return (
		// eslint-disable-next-line jsx-a11y/label-has-associated-control
		<label style={props.mb ? { marginBottom: props.mb } : {}} className={clsx(styles.root, props.className)}>
			{props.error ? (
				<span className={styles.error}>{props.error.message}</span>
			) : (
				props.label && (
					<span className={styles.inputName}>
						{props.label}
						{props.required ? <span className={styles.require}>*</span> : ''}
					</span>
				)
			)}
			<input
				readOnly={props.readOnly}
				value={props.value}
				type={type}
				disabled={props.disabled}
				error={props.error}
				size="small"
				className={clsx(styles.input, props.classNameInput)}
				{...(props.register || {})}
				onChange={e => {
					props.register?.onChange(e)
					props.onChange(e)
				}}
				placeholder={props.placeholder}
			/>
		</label>
	)
}

TextField.defaultProps = {
	onChange: () => {}
}
