import React, { useState } from 'react'
import Joi from 'joi'
import { useHistory, useLocation } from 'react-router-dom'
import clsx from 'clsx'
import * as queryString from 'query-string'
import { joiResolver } from '@hookform/resolvers/joi'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import { TRoutes, TApi, API_URL } from '../../shared/const'
import TextField from '../../UI/TextField'

import styles from './common.module.scss'

function repeatPasswordError(errors) {
	return errors.map(error => {
		// eslint-disable-next-line no-param-reassign
		if (error.code === 'any.only') {
			// eslint-disable-next-line no-param-reassign
			error.message = 'Repeat password does not match with Password'
		}
		return error
	})
}

export const schema = Joi.object({
	// eslint-disable-next-line newline-per-chained-call
	password: Joi.string().trim().min(8).max(128).required(),
	repeatPassword: Joi.any()
		.empty('')
		.equal(Joi.ref('password'))
		.required()
		.error(errors => repeatPasswordError(errors))
})

function NewPass() {
	const history = useHistory()
	const {
		register,
		handleSubmit,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})
	const [isLoading, setLoading] = useState(false)

	const location = useLocation()

	const { token, userId } = queryString.parse(location.search)

	const onSubmit = async form => {
		try {
			setLoading(true)
			await axios.post(API_URL + TApi.RESET_PASSWORD, {
				otp: token,
				userId,
				password: form.password,
				repeatPassword: form.repeatPassword
			})

			toast.success('Success')
			history.push(TRoutes.SIGN_IN)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		} finally {
			setLoading(false)
		}
	}

	return (
		<div className={styles.root}>
			<div className={styles.header}>
				<div className={styles.miniContainer}>
					<h2>Add new password </h2>
				</div>
			</div>
			<form className={clsx(styles.miniContainer, styles.form)} onSubmit={handleSubmit(onSubmit)} autoComplete="off">
				<TextField
					mb={22}
					type="password"
					label="New Password"
					placeholder="Your password"
					error={errors.password}
					register={register('password')}
					required
				/>

				<TextField
					mb={22}
					type="password"
					label="Repeat password"
					placeholder="Your password"
					error={errors.repeatPassword}
					register={register('repeatPassword')}
					required
				/>
				<Button type="submit" variant="contained" disabled={isLoading}>
					Change password
				</Button>
			</form>
		</div>
	)
}

export default NewPass
