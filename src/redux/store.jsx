import { configureStore } from '@reduxjs/toolkit'
import userReduser from './user'

export default configureStore({
	reducer: {
		user: userReduser
	}
})
