/* eslint-disable no-mixed-operators */
/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
/* eslint-disable prettier/prettier */
import { useState, useEffect } from 'react'
import joi from 'joi'
import { joiResolver } from '@hookform/resolvers/joi'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import http from '../../shared/http'
import { TApi } from '../../shared/const'
import TextField from '../../UI/TextField'
import Table from '../../UI/Table'
import MyTimer from '../../UI/Timer'

import styles from './style.module.scss'

export default function ItemBids(props) {
  const [auction, setAuction] = useState(props.location.state.auction)
  const [item, setItem] = useState(props.location.state.item)
  const [bids, setBids] = useState([])
  const [isLoading, setLoading] = useState(false)
  const [trigger, setTrigger] = useState(false)

  function onlyNaturalNumbers(value, helpers) {
    if (Math.floor(value) !== value) return helpers.message('Only natural numbers allowed!')

    return value
  }
  function test(value, helpers) {
    if (value * item.bidIncrement <= item.currentPrice) {
      return helpers.message('(Bid * increment) must be grater then current price!')
    }

    return value
  }
  const schema = joi.object({
    bid: joi
      .number()
      .min(1)
      .max(99999)
      .custom(test)
      .custom(onlyNaturalNumbers)
      .required(),
  })

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: joiResolver(schema),
    mode: 'onChange'
  })

  const userBid = watch('bid')

  function incrementBid(bid) {
    return bid * item.bidIncrement
  }

  async function onSubmit(form) {
    try {
      setLoading(true)
      const bid = incrementBid(form.bid)

      const isConfirmed = confirm('Are you sure you want to do this bid?')
      if (!isConfirmed) return

      await http.post(TApi.DO_BID, { itemId: item.id, bid })

      toast.succes('Bid has been added!')
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    } finally {
      setLoading(false)
      setTrigger(!trigger)
    }
  }

  useEffect(async () => {
    try {
      const result = await http.get(TApi.GET_ITEM, { params: { itemId: item.id } })
      const { item: auctionItem } = result.data.message
      setItem({ ...item, ...auctionItem })
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    }
  }, [trigger])

  useEffect(async () => {
    try {
      const result = await http.post(TApi.GET_BIDS, { itemId: item.id })
      const { bids: itemBids } = result.data.message
      setBids(itemBids)
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    }
  }, [trigger])

  useEffect(async () => {
    try {
      const result = await http.get(TApi.GET_AUCTION, { params: { auctionId: auction.id } })

      setAuction({ ...result.data.message.auction })
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    }
  }, [trigger])

  async function buyNow() {
    try {
      const isConfirmed = confirm('Are you sure you want to do this bid?')
      if (!isConfirmed) return

      await http.post(TApi.BUY_NOW, { itemId: item.id, bid: item.buyNowPrice })
      setTrigger(!trigger)
      toast.succes('Success!')
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    }
  }

  return (
    <div className={styles.root}>
      <div className={styles.wrapper}>
        <div className={styles.mainSection}>

          <div className={styles.imageSection}>
            <img
              style={{ height: '100%', maxWidth: 300 }}
              src={`data:image/png;base64,${item.image}`}
              alt="itemImage"
            />
          </div>

          <div className={styles.infoSection}>
            <h1 className={styles.itemName}>
              {item.name}
            </h1>

            <div style={{ width: '100%', maxWidth: 650, wordWrap: 'break-word' }}>
              <p style={{ fontSize: 14 }}>
                {item.description}
              </p>
            </div>

          </div>

        </div>

        <div style={{ padding: 10, maxWidth: 550, width: '100%' }}>
          <div className={styles.bidSection}>
            <div className={styles.bidContent}>
              <div className={styles.bidStatus}>
                <span>
                  Bid increment:
                  $
                  {item.bidIncrement}
                </span>
                <span className={styles.price}>
                  $
                  {item.currentPrice}
                </span>

              </div>
              <div style={{ marginBottom: '32px' }}>
                <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                  <TextField
                    mb={22}
                    type="number"
                    placeholder="Your bid"
                    error={errors.bid}
                    register={register('bid')}
                  />
                  <span>
                    Your bid wiil be:

                    {userBid && !errors.bid && ` $${userBid * item.bidIncrement}` || ' Nan'}
                  </span>
                  <Button
                    style={{ width: '100%' }}
                    type="submit"
                    variant="contained"
                    disabled={!auction.active || item.sold || isLoading}
                  >
                    BID
                  </Button>
                </form>
              </div>
            </div>

          </div>
          {item.buyNowPrice
        && (
        <div style={{ width: 500, padding: 15 }}>
          Buy on click

          <TextField
            mb={22}
            value={`$${item.buyNowPrice}`}
            type="text"
            placeholder="Value"
            disabled
          />

          <Button
            style={{ width: '100%', backgroundColor: '#21b6ae', }}
            variant="contained"
            onClick={() => buyNow()}
            disabled={!auction.active || item.sold
               || item.currentPrice >= item.buyNowPrice || isLoading}
          >
            BUY IN ONE CLICK
          </Button>
        </div>
        )}
        </div>

      </div>

      {auction.active && (
      <MyTimer
        expiryTimestamp={new Date(auction.endDate)}
        onExpire={() => setTrigger(!trigger)}
        className={styles.timer}
      />
      )}
      {!item.isAnonymBids
      && <Table data={bids} />}
    </div>
  )
}
