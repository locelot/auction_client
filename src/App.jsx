import { ToastContainer } from 'react-toastify'
import Dashboard from './components/Dashboard'
import Routers from './components/Routers'
import styles from './App.module.scss'
import 'react-toastify/dist/ReactToastify.css'
import 'react-datepicker/dist/react-datepicker.css'

function App() {
	return (
		<div className={styles.root}>
			<Dashboard />
			<Routers />
			<ToastContainer theme="colored" />
		</div>
	)
}

export default App
