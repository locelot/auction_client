import axios from 'axios'
import { TApi } from './const'

const instance = axios.create({
	baseURL: 'http://localhost:8080'
})

function refreshAccessToken() {
	const authTokens = localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : ''

	return axios.post(
		TApi.REFRESH_TOKEN,
		{},
		{
			headers: {
				Authorization: `Bearer ${authTokens.refreshToken}`
			}
		}
	)
}

instance.interceptors.request.use(
	config => {
		const authTokens = localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : ''
		if (authTokens) {
			config.headers.Authorization = `Bearer ${authTokens.accessToken}`
		}
		return config
	},
	error => {
		Promise.reject(error)
	}
)

instance.interceptors.response.use(
	result => result,
	async error => {
		const originalRequest = error.config
		if (error.response.status === 403 && !originalRequest._retry) {
			originalRequest._retry = true
			const result = await refreshAccessToken()
			const { accessToken, refreshToken } = result.data.message

			localStorage.setItem('authTokens', JSON.stringify({ accessToken, refreshToken }))
			originalRequest.headers.Authorization = `Bearer ${accessToken}`
			return instance(originalRequest)
		}
		return Promise.reject(error)
	}
)
export default instance
