/* eslint-disable prettier/prettier */
/* eslint-disable newline-per-chained-call */
import { useState } from 'react'
import Joi from 'joi'
import { joiResolver } from '@hookform/resolvers/joi'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import TextAreaField from '../../../UI/TextAreaField'
import http from '../../../shared/http'
import { TApi } from '../../../shared/const'
import TextField from '../../../UI/TextField'
import { joiFile } from '../../../shared/helpers'
import ImageUploader from '../../../UI/ImageUploader'

import styles from './style.module.scss'

const multipleOfBid = (startPrice, bitIncrement) => {
  if (startPrice % bitIncrement !== 0) return false
  return true
}

const schema = Joi.object({
  image: joiFile(10),
  name: Joi.string().required(),
  description: Joi.string().required(),
  bidIncrement: Joi.number().min(1).max(1000000).required(),
  startPrice: Joi.number().min(1).max(1000000).required(),
  buyNowPrice: Joi.number().greater(Joi.ref('startPrice')).allow('', null),
})

function CreateAuctionItem(props) {
  const {
    register,
    handleSubmit,
    setError,

    formState: { errors },
  } = useForm({
    resolver: joiResolver(schema),
  })

  const [imageSrc, setImageSrc] = useState('')
  const [auctionId] = useState(props.auctionId)
  const [isAnonymBids, setAnonymBids] = useState(true)
  const [isLoading, setLoading] = useState(false)

  const onSubmit = async (form) => {
    try {
      setLoading(true)
      const result = multipleOfBid(form.startPrice, form.bidIncrement)
      if (!result) {
        setError('startPrice', {
          type: 'custom',
          message:
            'The starting price should be a multiple of the bid increment',
        })
        return
      }

      const formImage = new FormData()
      if (form.image && form.image[0]) {
        formImage.append('image', form.image[0])
        setImageSrc(URL.createObjectURL(form.image[0]))
      }
      const data = await http.post(TApi.UPLOAD_ITEM_IMAGE, formImage)

      const { imageHashName } = data.data.message

      await http.post(TApi.CREATE_AUCTION_ITEM, {
        auctionId,
        imageHashName,
        name: form.name,
        description: form.description,
        bidIncrement: form.bidIncrement,
        startPrice: form.startPrice,
        buyNowPrice: form.buyNowPrice,
        isAnonymBids
      })

      toast.success('Item has been added')
      props.changeTab()
    } catch (err) {
      const { message } = err.response.data
      toast.error(message)
    } finally {
      setLoading(false)
    }
  }

  const imageChange = (e) => {
    setImageSrc(URL.createObjectURL(e.target.files[0]))
  }

  return (
    <div className={styles.root}>
      <form
        className={styles.form}
        onSubmit={handleSubmit(onSubmit)}
        autoComplete="off"
      >
        <div>
          <ImageUploader
            src={imageSrc}
            error={errors.image}
            // onClean={handleClean}
            onChange={imageChange}
            className={styles.ImageUploader}
            register={register('image')}
          />
        </div>
        <div>
          <TextField
            mb={22}
            label="Name"
            placeholder="Item name"
            error={errors.name}
            register={register('name')}
            required
          />
          <TextAreaField
            mb={22}
            height={200}
            maxLength={500}
            showLettersLeft
            label="Description"
            placeholder="Description"
            error={errors.description}
            register={register('description')}
            required
          />
          <div className={styles.flexContainer}>
            <TextField
              mb={22}
              type="number"
              label="Bid increment"
              placeholder="Bid increment"
              error={errors.bidIncrement}
              register={register('bidIncrement')}
              required
            />

            <TextField
              mb={22}
              type="number"
              label="Start price"
              placeholder="Start price"
              error={errors.startPrice}
              register={register('startPrice')}
              required
            />
          </div>

          <TextField
            mb={22}
            type="number"
            label="Buy-Now price"
            placeholder="Buy-Now price"
            error={errors.buyNowPrice}
            register={register('buyNowPrice')}
          />

          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Button type="submit" variant="contained" disabled={isLoading}>
              Create Item
            </Button>
          </div>
        </div>
        <div>
          <FormControlLabel
            control={(
              <Checkbox
                onChange={() => setAnonymBids(!isAnonymBids)}
                defaultChecked
              />
            )}
            label="Annonym bids"
          />
        </div>
      </form>
    </div>
  )
}

export default CreateAuctionItem
