import { useState } from 'react'
import Joi from 'joi'
import { Link, useHistory } from 'react-router-dom'
import clsx from 'clsx'
import { joiResolver } from '@hookform/resolvers/joi'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import Button from '@mui/material/Button'
import { toast } from 'react-toastify'
import { TRoutes, TApi, API_URL } from '../../../shared/const'
import TextField from '../../../UI/TextField'
import logo from '../../../assets/images/auth_logo1.jpg'

import styles from './style.module.scss'

const repeatPasswordError = errors =>
	errors.map(error => {
		if (error.code === 'any.only') error.message = 'Repeat password does not match with Password'
		return error
	})

const schema = Joi.object({
	phonenumber: Joi.string()
		.regex(/^(\+38)(0\d{9})$/)
		.required(),
	username: Joi.string().trim().min(4).max(16).required(),
	email: Joi.string()
		.email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
		.required(),
	password: Joi.string().trim().min(8).max(128).required(),
	repeat_password: Joi.any()
		.empty('')
		.equal(Joi.ref('password'))
		.required()
		.error(errors => repeatPasswordError(errors))
})

function SignUp() {
	const history = useHistory()
	const {
		register,
		handleSubmit,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})
	const [isLoading, setLoading] = useState(false)

	const onSubmit = async form => {
		try {
			setLoading(true)
			await axios.post(API_URL + TApi.SIGN_UP, {
				phonenumber: form.phonenumber,
				username: form.username,
				email: form.email,
				password: form.password,
				repeat_password: form.repeat_password
			})

			toast.success('Success')
			history.push(TRoutes.SIGN_IN)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		} finally {
			setLoading(false)
		}
	}

	return (
		<div className={styles.root}>
			<div className={styles.header}>
				<div className={styles.miniContainer}>
					<h2 className={styles.titleSignIn}>Sign Up</h2>
					<p className={styles.accountMessage}>
						Already have an accout?
						<Link to={TRoutes.SIGN_IN}> Sign In</Link>
					</p>
				</div>
			</div>

			<div className={styles.wrapper}>
				<div
					className={styles.logo}
					style={{
						backgroundImage: `url(${logo})`,
						backgroundRepeat: 'no-repeat',
						backgroundSize: '750px 550px'
					}}
				/>
				<form className={clsx(styles.miniContainer, styles.form)} onSubmit={handleSubmit(onSubmit)} autoComplete="off">
					<TextField
						mb={22}
						label="Phone"
						placeholder="Your phone"
						error={errors.phonenumber}
						register={register('phonenumber')}
						required
					/>
					<TextField
						mb={22}
						label="Email"
						placeholder="Your email"
						error={errors.email}
						register={register('email')}
						required
					/>

					<TextField
						mb={22}
						label="Username"
						placeholder="Your username"
						error={errors.username}
						register={register('username')}
						required
					/>

					<TextField
						mb={30}
						type="password"
						label="Password"
						placeholder="Your password"
						error={errors.password}
						register={register('password')}
						required
					/>
					<TextField
						mb={30}
						type="password"
						label="Repeat password"
						placeholder="Your password"
						error={errors.repeat_password}
						register={register('repeat_password')}
						required
					/>

					<div style={{ display: 'flex', justifyContent: 'center' }}>
						<Button style={{ width: '337px' }} type="submit" variant="contained" disabled={isLoading}>
							Sign Up
						</Button>
					</div>
				</form>
			</div>
		</div>
	)
}

export default SignUp
