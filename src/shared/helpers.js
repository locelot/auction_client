/* eslint-disable prettier/prettier */
import Joi from 'joi';
import prettyBytes from 'pretty-bytes';

export const megabytesToBytes = (megabytes) => megabytes * 1048576; // 1 Mb = 1048576 bytes

export const joiFile = (maxMegabytes) => Joi.custom((value, helper) => {
  if (!value[0]) return value[0];

  if (megabytesToBytes(maxMegabytes) < value[0].size) {
    return helper.message(
      `${'The file is too large. Current file size'}: ${prettyBytes(
        value[0].size
      )}. ${'Max file size is'} ${maxMegabytes}M.`
    );
  }
  return value;
});
