import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { TRoutes } from '../../shared/const'

export default function OnlyPublicRoute({ component: Component, ...rest }) {
	const isAuth = useSelector(state => state.user.isAuth)

	return <Route {...rest} render={() => (isAuth ? <Redirect to={TRoutes.AUCTION_LIST} /> : <Component />)} />
}
