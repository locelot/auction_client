FROM node:17.6.0

WORKDIR /usr/src/client

COPY ["package.json", "package-lock.json", "./"]

RUN npm set-script prepare '' && npm ci --only=production && npm cache clean -f

COPY ./ ./

EXPOSE 3000

CMD ["npm", "run", "start"]
