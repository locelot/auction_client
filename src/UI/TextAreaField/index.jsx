/* eslint-disable jsx-a11y/label-has-associated-control */
import { useState } from 'react'
import clsx from 'clsx'

import styles from './style.module.scss'

export default function TextAreaField(props) {
	const [count, setCount] = useState(0)

	return (
		<label style={props.mb ? { marginBottom: props.mb } : {}} className={clsx(styles.root, props.classNameLabel)}>
			<div className={styles.fieldHeader}>
				{props.error ? (
					<span className={styles.error}>{props.error.message}</span>
				) : (
					<span className={styles.inputName}>
						{props.label}
						{props.required ? <span className={styles.require}>*</span> : ''}
					</span>
				)}
				{props.showError && (
					<p className={styles.inputWarning}>
						<font color="#EB5757">Min 100</font>
						Symbols
					</p>
				)}
				{props.showLettersLeft && (
					<p className={styles.inputWarning}>
						<font color="#357B6B">{props.maxLength - count}</font>
						Symbols Left
					</p>
				)}
			</div>
			<textarea
				maxLength={props.maxLength}
				disabled={props.disabled}
				className={styles.input}
				style={props.height ? { height: `${props.height}px` } : {}}
				{...props.register}
				onChange={e => {
					props.register.onChange(e)
					setCount(e.target.value.length)
				}}
				placeholder={props.placeholder}
				name={props.register.name}
			/>
		</label>
	)
}

TextAreaField.defaultProps = {
	disabled: false
}
