import clsx from 'clsx'
import DatePicker from 'react-datepicker'
import { Controller } from 'react-hook-form'
import styles from './style.module.scss'

export default function DateTimeField(props) {
	// const handleDateChangeRaw = (e) => {
	//   e.preventDefault()
	// }
	return (
		// eslint-disable-next-line jsx-a11y/label-has-associated-control
		<label style={props.mb ? { marginBottom: props.mb } : {}} className={clsx(styles.root, props.className)}>
			{props.error ? (
				<span className={styles.error}>{props.error.message}</span>
			) : (
				<span className={styles.inputName}>
					{props.label}
					{props.required ? <span className={styles.require}>*</span> : ''}
				</span>
			)}
			<Controller
				name={props.name}
				control={props.control}
				render={({ field }) => (
					<DatePicker
						{...field}
						showYearDropdown
						showMonthDropdown
						yearDropdownItemNumber={14}
						// onChangeRaw={handleDateChangeRaw}
						popperClassName={styles.popperStyles}
						wrapperClassName={styles.datePicker}
						disabled={props.disabled}
						autoComplete="off"
						selected={field.value}
						isClearable
						onChange={date => field.onChange(date)}
						showTimeSelect={props.showTimeSelect}
						placeholderText={props.placeholder}
						dateFormat={props.dateFormat}
						timeFormat={props.timeFormat}
						minDate={props.minDate}
						maxDate={props.maxDate}
						filterTime={props.filterTime}
					/>
				)}
			/>
		</label>
	)
}

DateTimeField.defaultProps = {
	dateFormat: 'dd.MM.yyyy',

	disabled: false
}
