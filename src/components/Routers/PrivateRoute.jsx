import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { TRoutes } from '../../shared/const'

export default function PrivateRoute({ component: Component, ...rest }) {
	const isAuth = useSelector(state => state.user.isAuth)

	return (
		<Route
			{...rest}
			render={() => (isAuth ? <Route {...rest} component={Component} /> : <Redirect to={TRoutes.SIGN_IN} />)}
		/>
	)
}
