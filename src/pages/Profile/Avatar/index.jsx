import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { joiResolver } from '@hookform/resolvers/joi'
import Joi from 'joi'
import { toast } from 'react-toastify'

import ImageUploader from '../../../UI/ImageUploader'

import styles from './style.module.scss'
import http from '../../../shared/http'
import { TApi } from '../../../shared/const'
import { joiFile } from '../../../shared/helpers'

const schema = Joi.object({
	avatar: joiFile(10)
})

export default function uploadAvatar() {
	const {
		register,
		handleSubmit,
		clearErrors,
		formState: { errors }
		// eslint-disable-next-line react-hooks/rules-of-hooks
	} = useForm({
		reValidateMode: 'onChange',
		resolver: joiResolver(schema)
	})
	// eslint-disable-next-line react-hooks/rules-of-hooks
	const [avatarSrc, setAvatarSrc] = useState('')

	// eslint-disable-next-line react-hooks/rules-of-hooks
	useEffect(() => {
		async function fetchData() {
			try {
				const result = await http.get(TApi.GET_PROFILE_AVATAR)

				setAvatarSrc(`data:image/png;base64,${result.data.image}`)
			} catch (err) {
				const { message } = err.response.data
				toast.error(message)
			}
		}
		fetchData()
	}, [])

	const handleChange = async fields => {
		try {
			const form = new FormData()
			if (fields.avatar && fields.avatar[0]) {
				form.append('image', fields.avatar[0])
			}
			setAvatarSrc(URL.createObjectURL(fields.avatar[0]))
			await http.post(TApi.UPDATE_PROFILE_AVATAR, form)
			toast.success('Image has been successfully uploaded')
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}

	const handleClean = () => {
		if (avatarSrc) {
			http.delete(TApi.STUDENT_Image_DELETE).then(() => {
				toast.success('Image successfully deleted')
				setAvatarSrc('')
				clearErrors('Image')
			})
		}
	}

	return (
		<div className={styles.root}>
			<ImageUploader
				src={avatarSrc}
				error={errors.avatar}
				onClean={handleClean}
				onChange={handleSubmit(handleChange)}
				className={styles.ImageUploader}
				register={register('avatar')}
			/>
		</div>
	)
}
