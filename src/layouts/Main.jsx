import { Route, Switch } from 'react-router-dom'
import { TRoutes } from '../shared/const'
import ListAuctions from '../pages/ListAuctions'
import CreateAuctionItem from '../pages/Auction/CreateAuctionItem'
import Profile from '../pages/Profile'
import AuctionMain from '../pages/Auction'
import ItemBids from '../pages/ItemBidding'
import NotFound from '../pages/NotFound'

export default function Main() {
	return (
		<Switch>
			<Route path={TRoutes.AUCTION} component={AuctionMain} exact />
			<Route path={TRoutes.AUCTION_LIST} component={ListAuctions} exact />
			<Route path={TRoutes.CREATE_AUCTION_ITEM} component={CreateAuctionItem} exact />
			<Route path={TRoutes.PROFILE} component={Profile} exact />
			<Route path={TRoutes.BIDS} component={ItemBids} exact />
			<Route path="*" component={NotFound} />
		</Switch>
	)
}
