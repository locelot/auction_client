export const API_URL = 'http://localhost:8080'

export class TRoutes {
	static NOT_FOUND = '/not-found'

	static MAIN = '/'

	static AUTH = '/auth'

	static AUCTION = '/auction'

	static PROFILE = '/user/profile'

	static SIGN_IN = `${TRoutes.AUTH}/signin`

	static SIGN_UP = `${TRoutes.AUTH}/signup`

	static RECOVER_PASS = `${TRoutes.AUTH}/request-password-reset`

	static NEW_PASS = `${TRoutes.AUTH}/reset-password`

	static AUCTION_LIST = `${TRoutes.AUCTION}/list`

	static CREATE_AUCTION_ITEM = `${TRoutes.AUCTION}/create-item`

	static BIDS = '/item/bid'
}

export class TApi {
	static SIGN_IN = '/api/public/auth/signin'

	static SIGN_UP = '/api/public/auth/signup'

	static RESET_PASSWORD = '/api/public/auth/forgot-password/reset-password'

	// eslint-disable-next-line prettier/prettier
  static REQUEST_PASSWORD_RESET = '/api/public/auth/forgot-password/request-password-reset';

	static REFRESH_TOKEN = `${API_URL}/api/public/auth/refresh_token`

	static GET_AUCTIONS = '/api/private/auction/get-auctions'

	static CREATE_AUCTION = '/api/private/auction/create-auction'

	static REMOVE_AUCTION = '/api/private/auction/remove-auction'

	static GET_AUCTION_ITEMS = '/api/private/auction/get-auction-items'

	static CREATE_AUCTION_ITEM = '/api/private/auction/create-item'

	static REMOVE_ITEM = '/api/private/auction/remove-item'

	static UPLOAD_ITEM_IMAGE = `${API_URL}/api/private/auction/upload-item-image`

	static GET_ITEM_IMAGE = '/api/private/auction/get-item-image'

	static GET_ALL_ITEM_IMAGES = '/api/private/auction/get-all-items-image'

	static GET_IMAGES = `${API_URL}/api/private/image/get-images`

	static GET_PROFILE = '/api/private/profile/get-profile'

	static UPDATE_PROFILE = '/api/private/profile/update-profile'

	static GET_PROFILE_AVATAR = '/api/private/profile/get-avatar'

	static UPDATE_PROFILE_AVATAR = '/api/private/profile/update-avatar'

	static DO_BID = '/api/private/auction/do-bid'

	static GET_BIDS = '/api/private/auction/get-bids'

	static GET_ITEM = '/api/private/auction/get-item'

	static GET_AUCTION = '/api/private/auction/get-auctionById'

	static GET_AUCTION_CREATOR = '/api/private/auction/get-auction-creator'

	static GET_AUCTION_ACCESS = '/api/private/auction/auction-access'

	static UPDATE_AUCTION = '/api/private/auction/update-auction'

	static BUY_NOW = '/api/private/auction/buy-now'
}
