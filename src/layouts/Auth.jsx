import { Route, Redirect, Switch } from 'react-router-dom'

import SignIn from '../pages/Auth/SignIn'
import SignUp from '../pages/Auth/SignUp'
import NewPass from '../pages/Auth/NewPass'
import RecoverPass from '../pages/Auth/RecoverPass'

import { TRoutes } from '../shared/const'

export default function Auth() {
	return (
		<Switch>
			<Route path={TRoutes.SIGN_IN} component={SignIn} exact />
			<Route path={TRoutes.SIGN_UP} component={SignUp} exact />
			<Route path={TRoutes.NEW_PASS} component={NewPass} exact />
			<Route path={TRoutes.RECOVER_PASS} component={RecoverPass} exact />
			<Redirect to={TRoutes.SIGN_IN} exact />
		</Switch>
	)
}
