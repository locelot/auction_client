/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
import clsx from 'clsx'
import { useState, useEffect } from 'react'
import { toast } from 'react-toastify'
import axios from 'axios'
import { Link } from 'react-router-dom'
import DeleteIcon from '@mui/icons-material/AutoDelete'
import Button from '@mui/material/Button'
import http from '../../../shared/http'
import { TRoutes, API_URL, TApi } from '../../../shared/const'

import styles from './style.module.scss'

export default function AuctionItem(props) {
	const [item] = useState(props.item)
	const [image, setImage] = useState('')

	async function removeItem() {
		try {
			await http.post(TApi.REMOVE_ITEM, {
				itemId: item.id
			})
			props.onDelete(item.id)
			toast.success('Item has been removed!')
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}

	useEffect(async () => {
		try {
			const result = await axios.post(API_URL + TApi.GET_ITEM_IMAGE, {
				itemId: item.id
			})
			setImage(result.data.image)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}, [item])

	return (
		<div className={styles.auctionBlock}>
			<div className={styles.image}>
				<span className={styles.deleteIcon}>
					{props.rightToDelete && (
						<DeleteIcon
							color="error"
							onClick={() => {
								const confirmation = confirm('Are you sure you want to delete auction?')
								if (!confirmation) return
								removeItem()
							}}
						/>
					)}
				</span>
				<img style={{ height: '100%' }} src={`data:image/png;base64,${image}`} alt="itemImage" />
			</div>
			<div className={styles.info}>
				<div>
					<span className={clsx(styles.infoBlock, styles.infoName)}>{item && item.name}</span>
					<span className={clsx(styles.infoBlock, styles.infoDate)}>Some info</span>
				</div>
				<Link
					style={{ textDecoration: 'none' }}
					to={{
						pathname: TRoutes.BIDS,
						state: {
							auction: props.auction,
							item: {
								...item,
								image
							}
						}
					}}>
					<Button type="button" variant="contained">
						Explore
					</Button>
				</Link>
			</div>
		</div>
	)
}
