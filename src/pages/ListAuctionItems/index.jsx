/* eslint-disable operator-linebreak */
import { useState, useEffect } from 'react'
import { joiResolver } from '@hookform/resolvers/joi'
import { useForm, useWatch } from 'react-hook-form'
import Joi from 'joi'
import { toast } from 'react-toastify'
import { useSelector } from 'react-redux'
import Avatar from 'react-avatar'
import http from '../../shared/http'
import { TApi } from '../../shared/const'
import AuctionItemPreview from './ItemPreview'
import MyTimer from '../../UI/Timer'
import TextField from '../../UI/TextField'
import { ReactComponent as NotFoundSVG } from '../../assets/icons/data-not-found.svg'

import styles from './style.module.scss'

function ListAuctionItems(props) {
	const { auction } = props
	const userId = useSelector(state => state.user.data.id)
	const [auctionId] = useState(props.auction.id)
	const [items, setItems] = useState([])
	const [creatorAvatar, setCreatorAvatar] = useState('')
	const [creator, setCreator] = useState({})

	const schema = Joi.object({
		byCategory: Joi.string().required()
	})

	const {
		register,
		control,
		getValues,
		formState: { errors }
	} = useForm({
		resolver: joiResolver(schema)
	})

	const [search, setSearch] = useState('')

	const filters = useWatch({
		control,
		name: ['byCategory']
	})

	function handleDeleteItem(itemId) {
		const updatedItems = items.filter(e => e.id !== itemId)
		setItems(updatedItems)
	}

	useEffect(async () => {
		try {
			const filterOptions = getValues()
			const result = await http.get(TApi.GET_AUCTION_ITEMS, {
				params: {
					search,
					filter: JSON.stringify({ ...filterOptions }),
					auctionId
				}
			})

			setItems(result.data.message.items)
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}, [search, filters])

	useEffect(async () => {
		try {
			const result = await http.get(TApi.GET_AUCTION_CREATOR, {
				params: {
					creatorId: auction.creator_id
				}
			})

			const avatar = await http.get(TApi.GET_PROFILE_AVATAR, {
				params: {
					userId: auction.creator_id
				}
			})

			setCreatorAvatar(avatar.data.image)
			setCreator({ ...result.data.message.creator })
		} catch (err) {
			const { message } = err.response.data
			toast.error(message)
		}
	}, [])

	function dataList(data) {
		return data.map(item => (
			<AuctionItemPreview
				key={item.id}
				item={item}
				auction={props.auction}
				onDelete={handleDeleteItem}
				rightToDelete={!auction.active && userId === auction.creatorId}
			/>
		))
	}

	return (
		<div className={styles.root}>
			<div>{auction.active && <MyTimer expiryTimestamp={new Date(auction.endDate)} className={styles.timer} />}</div>

			<div className={styles.filter}>
				<TextField
					label="Category"
					placeholder="Category"
					error={errors.byCategory}
					register={register('byCategory')}
				/>

				<TextField placeholder="Search" onChange={e => setSearch(e.target.value)} />
			</div>
			{creator && (
				<div style={{ display: 'flex', marginTop: 25 }}>
					<Avatar
						src={`data:image/png;base64,${creatorAvatar}`}
						className={styles.preview}
						color="#e5eaf5"
						round="50px"
						size={80}
					/>
					<div>
						<h2 style={{ padding: 0, margin: 0 }}>{creator.username}</h2>
						<span>
							{creator.firstname}
							{` ${creator.surname}`}
						</span>
					</div>
				</div>
			)}

			<div className={styles.items}>{items.length ? dataList(items) : <NotFoundSVG style={{ width: 200 }} />}</div>
		</div>
	)
}

export default ListAuctionItems
