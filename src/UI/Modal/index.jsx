import { useEffect, useState } from 'react'
import Box from '@mui/material/Box'
import Modal from '@mui/material/Modal'
import { Emitter as eventEmitter } from '../../shared/eventEmitter'

const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	maxWidth: 450,
	width: '100%',
	bgcolor: 'background.paper',
	borderRadius: '5px',
	boxShadow: 24,
	p: 4
}

export default function BasicModal({ component: Component, eventName, ...rest }) {
	const [open, setOpen] = useState(false)
	const handleOpen = () => setOpen(true)
	const handleClose = () => {
		setOpen(false)
	}

	useEffect(() => {
		eventEmitter.on(eventName, handleOpen)
		return () => {
			eventEmitter.off(eventName, handleOpen)
		}
	}, [eventName])

	if (!open) return null

	return (
		<Modal
			open={open}
			onClose={handleClose}
			aria-labelledby="modal-modal-title"
			aria-describedby="modal-modal-description">
			<Box sx={style}>
				<Component handleClose={handleClose} {...rest} />
			</Box>
		</Modal>
	)
}
