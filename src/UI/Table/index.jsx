/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable prettier/prettier */
/* eslint-disable no-param-reassign */
import { useMemo } from 'react'
import {
  useTable, useSortBy, usePagination, useFlexLayout
} from 'react-table'

import style from './style.module.scss'

export default function Table({ columns, data }) {
  columns = useMemo(() => [
    {
      Header: 'Name',
      accessor: 'userName',

    },
    {
      Header: 'Time',
      accessor: 'createdAt',
      Cell: ({ row }) => <p>{new Date(row.original.createdAt).toLocaleString()}</p>

    },
    {
      Header: 'Bid',
      accessor: 'bid',
      Cell: ({ row }) => <p>{`$${row.original.bid}`}</p>

    },
  ], [])

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,

  } = useTable(
    {
      columns,
      data,
      manualSortBy: true
    },
    useSortBy,
    usePagination,
    useFlexLayout
  )

  return (
    <div className={style.root}>
      <div className={style.tableWrapper}>
        <div {...getTableProps()} className={style.table}>
          <div>
            {headerGroups.map((headerGroup) => (
              <div {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <div
                    className={style.title}
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                  >
                    {column.render('Header')}

                  </div>
                ))}
              </div>
            ))}
          </div>
          <div {...getTableBodyProps()}>
            {rows.map((row) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()} className={style.row}>
                  {row.cells.map((cell) => (
                    <td {...cell.getCellProps()} className={style.cell}>
                      {cell.render('Cell')}
                    </td>
                  ))}
                </tr>
              )
            })}
          </div>
        </div>
      </div>

    </div>
  )
}
