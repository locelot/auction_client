import { Route, Switch, Redirect } from 'react-router-dom'

import PrivateRoute from './PrivateRoute'
import OnlyPublicRoute from './OnlyPublicRoute'

import NotFound from '../../pages/NotFound'

import { TRoutes } from '../../shared/const'

import Auth from '../../layouts/Auth'
import Main from '../../layouts/Main'

function Routers() {
	return (
		<Switch>
			<OnlyPublicRoute path={TRoutes.AUTH} component={Auth} />
			<PrivateRoute path={TRoutes.MAIN} component={Main} />

			{/* <Route path="/404" component={<div>Choose the correct path</div>} /> */}
			{/* <Redirect from="*" to={<Navigate replace to="/404" />} /> */}
			<Route path="*" component={NotFound} />
			{/* <Redirect exact to={TRoutes.NOT_FOUND} /> */}
		</Switch>
	)
}

export default Routers
